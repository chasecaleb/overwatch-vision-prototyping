#!/usr/bin/env bash
# Downloads and installs OpenCV (including contrib modules)

VERSION=3.4.2
# Building in home dir instead of /tmp because opencv is massive
# (And thus won't fit in a modest-sized tmpfs, which is common for /tmp)
BUILD_DIR=~/.cache/opencv-build-tmp

function die() {
    echo "ERROR: $1"
    exit 1
}

function cleanup() {
    echo "Cleaning up build files..."
    rm -r $BUILD_DIR
}

trap cleanup EXIT

if [[ -d $BUILD_DIR ]]; then
    rm -r $BUILD_DIR
fi
mkdir $BUILD_DIR || die "Could not create build dir: $BUILD_DIR"
cd $BUILD_DIR

curl https://github.com/opencv/opencv/archive/$VERSION.tar.gz \
     --location \
     --output opencv.tar.gz \
    || die "Failed to download opencv"
curl https://github.com/opencv/opencv_contrib/archive/$VERSION.tar.gz \
     --location \
     --output opencv_contrib.tar.gz \
    || die "Failed to download opencv_contrib"

tar xvf opencv.tar.gz || die "Failed extracting opencv"
tar xvf opencv_contrib.tar.gz || die "Failed extracting opencv_contrib"

cd $BUILD_DIR/opencv-$VERSION || die "Failed switching to extracted dir"
mkdir build
cd build

# NOTE: may need these:
cmake \
    -D CMAKE_BUILD_TYPE=RelWithDebInfo \
    -D CMAKE_INSTALL_PREFIX=/opt/opencv \
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-$VERSION/modules \
    -D BUILD_PERF_TESTS=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_DOCS=OFF \
    -D BUILD_WITH_DEBUG_INFO=ON \
    -D WITH_GTK=ON \
    -D WITH_QT=ON \
    -D WITH_TBB=ON \
    -D OpenGL_GL_PREFERENCE=GLVND \
    -DLAPACK_LIBRARIES="/usr/lib/liblapack.so;/usr/lib/libblas.so;/usr/lib/libcblas.so" \
    -DLAPACK_CBLAS_H="/usr/include/cblas.h" \
    -DLAPACK_LAPACKE_H="/usr/include/lapacke.h" \
    ../ || die "cmake failed"
make -j4 || die "compile failed"
make install || die "install failed"
