import random
import archive_data


class Sample:
    def __init__(self, image, label):
        self.image = image
        self.label = label


class SampleManager:
    def __init__(self, samples, batch_size, validation_split=0.2):
        self._samples = samples
        self._batch_size = batch_size
        self._validation_split = validation_split

    def shuffle(self):
        random.shuffle(self._samples)

    def training_batches(self):
        for i in range(0, self._training_len(), self._batch_size):
            yield self._samples[i:i + self._batch_size]

    def validation_batches(self):
        for i in range(self._training_len(), len(self._samples),
                       self._batch_size):
            end = i + self._batch_size
            # Batches need to be same size, so skip last one if too small.
            if (end < len(self._samples)):
                yield self._samples[i:end]

    def _training_len(self):
        return int(len(self._samples) * (1 - self._validation_split))


def load_samples(sample_archive, batch_size=50):
    data = archive_data.load(sample_archive)
    images = data["images"]
    labels = data["labels"]
    assert len(images) == len(labels)
    samples = [Sample(images[i], labels[i]) for i in range(len(images))]
    return SampleManager(samples, batch_size)
