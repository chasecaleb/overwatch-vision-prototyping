#!/usr/bin/env python3
import glob
import os
import h5py
import numpy as np
import cv2

EMPTY_LABEL = "empty"


def load_image(filename):
    """ Load single image from file and preprocess. """
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    # Apparently TensorFlow needs images transposed.
    img = cv2.transpose(img)
    # Normalize.
    (m, s) = cv2.meanStdDev(img)
    m = m[0][0]
    s = s[0][0]
    img = img - m
    img = img / s if s > 0 else img
    return img


def _to_label(filename):
    """ Image filenames are formatted as [value].[uuid].png"""
    prefix = os.path.basename(filename).split(".")[0]
    if prefix == EMPTY_LABEL:
        return " "
    return prefix


def _save_h5(filename, images, labels):
    with h5py.File(filename, "w") as hf:
        hf.create_dataset("images", data=images)
        hf.create_dataset("labels", data=labels)


def archive(source_dir, output):
    img_files = glob.glob(f"{source_dir}/*png")
    imgs = np.array([load_image(f) for f in img_files])
    # astype("S") to use ASCII encoding (instead of unicode).
    labels = np.array([_to_label(f) for f in img_files]).astype("S")
    _save_h5(output, imgs, labels)


def load(filename):
    """ Load training dataset from h5 file. """
    result = {}
    with h5py.File(filename, "r") as hf:
        result["images"] = hf["images"].value
        # Convert back to unicode (Python 3 strings)
        result["labels"] = hf["labels"].value.astype("U")
    return result
