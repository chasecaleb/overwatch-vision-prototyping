#!/usr/bin/env python3
import argparse
import re
import os
import subprocess
import random
import uuid
import archive_data


def rand_stat_value():
    # Add some empty images
    if (random.random() < 0.05):
        return " "

    # One of six stats is time formatted as "mm:ss"
    if (random.randint(0, 5) == 0):
        mins = f"{random.randint(0, 30):02}"
        secs = f"{random.randint(0, 59):02}"
        return f"{mins}:{secs}"
    else:
        num_digits = random.randint(1, 5)
        min_val = 0 if num_digits == 1 else 10**(num_digits - 1)
        max_val = (10**num_digits) - 1
        # Stats have comma for thousands separator.
        # Seems like it might be a period in e.g. UK, but can't find any
        # examples of that on Google so maybe not. Either way, a comma and a
        # period look pretty similar so whatever.
        return "{:,}".format(random.randint(min_val, max_val))


def hex_colors(max_bg, min_fg, separation):
    """ [background, text] hex color strings. """

    # RGB channels all same because output is grayscale, so doesn't matter.
    def to_hex(value):
        return "#" + "".join(["%02X" % value for _ in range(3)])

    bg = random.randint(0, max_bg)
    fg = random.randint(max(bg + separation, min_fg), 255)
    return [to_hex(bg), to_hex(fg)]


def rand_font():
    # Using multiple to avoid overfitting, but biasing towards the real font.
    # Honestly not sure how much this helps, if at all, but came across this
    # suggestion somewhere online and it seemed logical.
    # bignoodletoo is used for stat values, koverwatch used elsewhere.
    font = "bignoodletoo.ttf" if random.random() < 0.66 else "koverwatch.ttf"
    return "../resources/" + font


def rand_perspective(width, height):
    def rand_offset(value):
        return random.uniform(value - 4, value + 4)

    def rand_point(x, y):
        return f"{x},{y} {rand_offset(x)},{rand_offset(y)}"

    return " ".join([
        rand_point(0, 0),
        rand_point(0, height),
        rand_point(width, height),
        rand_point(width, 0)
    ])


def distort_args(width, height):
    # TODO: Add moire effect? Not sure how to do with imagemagick.
    # (But I'm sure it can be done, I just didn't try hard.)

    # Affine transforms (e.g. scale, translate, rotate, shear) are a subset of
    # perspective transform, so those are covered here too.
    cmd = ["-distort", "Perspective", rand_perspective(width, height)]
    # Motion blur is relative to the camera, not the scene, so it goes *after*
    # perspective distortion.
    blur_amount = random.uniform(0, 2.5)
    blur_angle = random.uniform(0, 360)
    cmd += ["-motion-blur", f"0x{blur_amount}+{blur_angle}"]
    # Noise goes at the very end because it's caused by the camera sensor.
    cmd += ["-attenuate", str(random.uniform(0, 0.75)), "+noise", "Gaussian"]
    return cmd


def generate_stat(output_dir):
    image_value = rand_stat_value()
    # Strip commas, colons, etc from file label - don't care about them.
    label_value = re.sub(r"[^0-9]", "", image_value)
    if (label_value == ""):
        label_value = archive_data.EMPTY_LABEL
    filename = f"{output_dir}/{label_value}.{uuid.uuid4()}.png"

    cmd = ["convert", "-size", "64x32", "-gravity", "center"]
    (bg_color, text_color) = hex_colors(100, 150, 85)
    cmd += ["xc:" + bg_color]
    cmd += ["-font", rand_font()]
    # Stats are ~24pt font when screen fills 90% of a 1280x720 picture.
    # (I took a screenshot from PC version, resized, and measured it.)
    cmd += ["-pointsize", "24"]
    cmd += ["-fill", text_color]
    cmd += ["-annotate", "+0+0", image_value]
    cmd += distort_args(64, 32)
    cmd += ["-colorspace", "gray"]
    cmd += [filename]
    subprocess.Popen(cmd)


# TODO: replace string labels with ordinal integers?
def generate_outcome(output_dir):
    outcome = random.choice(["victory", "defeat", "draw"])
    map_name = random.choice([
        "blizzard world", "busan", "dorado", "eichenwalde", "hanamura",
        "hollywood", "horizon lunar colony", "ilios", "junkertown",
        "king's row", "lijiang tower", "nepal", "numbani", "oasis", "rialto",
        "route 66", "temple of anubis", "volskaya industries",
        "watchpoint: gibraltar"
    ])
    map_label = re.sub(r"[^a-z]", "", map_name)
    filename = f"{output_dir}/{outcome}-{map_label}.{uuid.uuid4()}.png"

    # TODO might need to allow for less separation - red defeat is dark.
    (bg_color, text_color) = hex_colors(100, 150, 85)
    # This command is ridiculous and there's probably a "better" way, but I
    # have no idea what. ImageMagick's CLI is uh... unique.
    cmd = ["convert", "-background", bg_color, "-fill", text_color]
    # TODO use other/multiple fonts?
    cmd += ["-font", "../resources/Futura.ttf"]

    # First part: outcome
    # Trailing space is escaped to prevent imagemagick from stripping.
    cmd += ["-pointsize", "48", f"label:{outcome.upper()}\\ "]
    # Second part: map.
    # This gets crazy because, well... imagemagick.
    # Parentheses + border for vertical alignment relative to outcome.
    cmd += ["(", "-respect-parentheses"]
    cmd += ["-bordercolor", bg_color, "-border", "0x5"]
    cmd += ["-pointsize", "20", f"label:{map_name.upper()}"]
    cmd += [")"]
    # Merge them together, side by side.
    cmd += ["+append", "-gravity", "center", "-background", bg_color]
    cmd += ["-bordercolor", bg_color, "-border", "10"]
    size = "192x32"
    cmd += ["-resize", size, "-extent", size]

    cmd += distort_args(192, 32)
    cmd += ["-colorspace", "gray"]
    cmd += [filename]
    subprocess.Popen(cmd)


# TODO: replace string labels with ordinal integers?
def generate_hero(output_dir):
    hero = random.choice([
        "d.va", "orisa", "reinhardt", "roadhog", "winston", "wrecking ball",
        "zarya", "bastion", "doomfist", "genji", "hanzo", "junkrat", "mccree",
        "mei", "pharah", "reaper", "solider: 76", "sombra", "symmetra",
        "torbjorn", "tracer", "widowmaker", "ana", "brigitte", "lucio",
        "mercy", "moira", "zenyatta"
    ])
    hero_label = re.sub(r"[^a-z]", "", hero)
    filename = f"{output_dir}/{hero_label}.{uuid.uuid4()}.png"

    cmd = ["convert", "-size", "400x400", "-gravity", "center"]
    (bg_color, text_color) = hex_colors(100, 150, 85)
    cmd += [f"xc:{bg_color}", "-fill", text_color]
    # TODO: multiple fonts?
    cmd += ["-font", "../resources/bignoodletoo.ttf", "-pointsize", "64"]
    cmd += ["-annotate", "+0+0", hero.upper()]
    cmd += ["-trim", "+repage", "-bordercolor", bg_color, "-border", "15"]
    cmd += ["-background", bg_color]
    size = "192x32"
    cmd += ["-resize", size, "-extent", size]
    cmd += distort_args(192, 32)

    cmd += ["-colorspace", "gray"]
    cmd += [filename]
    subprocess.Popen(cmd)


if __name__ == "__main__":
    proc = subprocess.run(["which", "convert"], stdout=subprocess.DEVNULL)
    if proc.returncode != 0:
        print("ERROR: imagemagick not installed")
        exit(1)

    parser = argparse.ArgumentParser("Generate training images")
    parser.add_argument(
        "-c",
        "--count",
        dest="count",
        help="number to create",
        type=int,
        required=True)
    parser.add_argument(
        "-d",
        "--dest",
        dest="dest",
        default="./data",
        help="destination directory")

    args = parser.parse_args()

    stats_dir = os.path.join(args.dest, "stats")
    outcomes_dir = os.path.join(args.dest, "outcomes")
    heroes_dir = os.path.join(args.dest, "heroes")
    os.makedirs(stats_dir)
    os.makedirs(outcomes_dir)
    os.makedirs(heroes_dir)

    for i in range(args.count):
        generate_stat(stats_dir)
        generate_outcome(outcomes_dir)
        generate_hero(heroes_dir)
        if i % 100 == 0:
            print(f"{i:,}/{args.count:,}")

    print("Generating archives...")
    archive_data.archive(stats_dir,
                         os.path.join(args.dest, "training-data-stats.h5"))
    archive_data.archive(outcomes_dir,
                         os.path.join(args.dest, "training-data-outcomes.h5"))
    archive_data.archive(outcomes_dir,
                         os.path.join(args.dest, "training-data-heroes.h5"))
    print("Done")
