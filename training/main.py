#!/usr/bin/env python3
import argparse
import os
import editdistance
import sample_manager
import archive_data
from stats_model import StatsModel
from text_mapper import TextMapper

# Stats are numeric, so of course possible text is 0-9.
# (Plus a space to represent empty/nothing.)
CHAR_LIST = " 0123456789"
# Maximum consecutive epochs without improvement before aborting.
MAX_STALE_EPOCHS = 15


def train(model, samples):
    epoch = 0
    best_error = float("inf")
    last_improvement = 0

    while True:
        epoch += 1
        print(f"Epoch: {epoch:5}")

        samples.shuffle()
        for (i, batch) in enumerate(samples.training_batches()):
            loss = model.train_batch(batch)
            print(f"\tBatch: {i:5}, loss: {loss:.9f}")

        current_error = validate(model, samples)
        print(f"Current error: {current_error:.9f}, best: {best_error:.9f}")
        if (current_error < best_error):
            best_error = current_error
            model.save()
            last_improvement = 0
        else:
            last_improvement += 1

        if best_error < 0.5 and last_improvement == MAX_STALE_EPOCHS:
            print(f"FINISHED: no improvements in {last_improvement} epochs")
            break

    print(f"Final error rate: {best_error}")


def validate(model, samples):
    char_err = 0
    char_total = 0

    for batch in samples.validation_batches():
        recognized = model.infer_batch(batch)
        for i in range(len(recognized)):
            char_total += len(batch[i].label)
            dist = editdistance.eval(recognized[i], batch[i].label)
            if dist == 0:
                print(f"[OK]: {batch[i].label}")
            else:
                print(f"[ERR]: result: {recognized[i]:7}, "
                      f"actual: {batch[i].label:7}")
            char_err += dist

    return char_err / char_total


def infer(model, image_filenames):
    images = [archive_data.load_image(f) for f in image_filenames]
    samples = [sample_manager.Sample(img, None) for img in images]
    return model.infer_batch(samples)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Model training")
    parser.add_argument("--train", action="store_true")
    parser.add_argument("infer", nargs="*", help="Image(s) to infer")
    args = parser.parse_args()

    mapper = TextMapper(CHAR_LIST)
    if args.train:
        model = StatsModel(mapper)
        samples = sample_manager.load_samples("./data/training-data-stats.h5")
        train(model, samples)
    elif args.infer:
        model = StatsModel(mapper, True)
        results = infer(model, args.infer)
        for (i, res) in enumerate(results):
            print(f"{res:10} | {os.path.basename(args.infer[i])}")
    else:
        print("Argument required (use -h for help)")
        exit(1)
