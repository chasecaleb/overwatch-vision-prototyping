import tensorflow as tf


class StatsModel:
    IMG_W = 64
    IMG_H = 32
    # Max length needs to be doubled to handle repeated digits
    # Example: CTC represents "112" as [1, blank, 1, 2]
    # ... Or at least I think that's the reason.
    # https://stackoverflow.com/questions/45568266/tensorflow-ctc-loss-ctc-merge-repeated-parameter
    MAX_CHARS = 10
    MODEL_DIR = "./model-output"

    def __init__(self, text_mapper, must_restore=False):
        self._text_mapper = text_mapper
        self._must_restore = must_restore
        self._snapshot_id = 0

        self._input_imgs = tf.placeholder(
            tf.float32, shape=(None, StatsModel.IMG_W, StatsModel.IMG_H))
        cnn_out_4d = self._init_CNN()
        rnn_out_3d = self._init_RNN(cnn_out_4d)
        (self._loss, self._decoder) = self._init_ctc(rnn_out_3d)

        self._batches_trained = 0
        self._learning_rate = tf.placeholder(tf.float32, shape=[])
        self._optimizer = tf.train.RMSPropOptimizer(
            self._learning_rate).minimize(self._loss)

        (self._sess, self._saver) = self._init_tf()

    def _init_CNN(self):
        kernel_vals = [5, 5, 3, 3, 3]
        feature_vals = [1, 32, 64, 128, 128, 256]
        stride_vals = pool_vals = [(2, 2), (2, 2), (1, 2), (1, 2), (1, 2)]

        pool = tf.expand_dims(input=self._input_imgs, axis=3)
        num_layers = len(stride_vals)
        for i in range(num_layers):
            kernel = tf.Variable(
                tf.truncated_normal([
                    kernel_vals[i], kernel_vals[i], feature_vals[i],
                    feature_vals[i + 1]
                ],
                                    stddev=0.1))
            conv = tf.nn.conv2d(
                pool, kernel, padding='SAME', strides=(1, 1, 1, 1))
            relu = tf.nn.relu(conv)
            pool = tf.nn.max_pool(
                relu, (1, pool_vals[i][0], pool_vals[i][1], 1),
                (1, stride_vals[i][0], stride_vals[i][1], 1), "VALID")

        return pool

    def _init_RNN(self, input_4d):
        input_squeezed = tf.squeeze(input_4d, axis=[2])

        # Basic cells used to build RNN - 2 layers
        num_hidden = 256
        cells = [
            tf.contrib.rnn.LSTMCell(num_units=num_hidden, state_is_tuple=True)
            for _ in range(2)
        ]
        stacked = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=True)

        # bidirectional RNN: BxTxF -> BxTx2H
        ((fw, bw), _) = tf.nn.bidirectional_dynamic_rnn(
            cell_fw=stacked,
            cell_bw=stacked,
            inputs=input_squeezed,
            dtype=input_squeezed.dtype)

        # BxTxH + BxTxH -> BxTx2H -> BxTx1X2H
        concat = tf.expand_dims(tf.concat([fw, bw], 2), 2)

        # Project output to chars (w/ blank): BxTx1x2H -> BxTx1xC -> BxTxC
        kernel = tf.Variable(
            tf.truncated_normal(
                [1, 1, num_hidden * 2,
                 self._text_mapper.token_count()],
                stddev=0.1))
        return tf.squeeze(
            tf.nn.atrous_conv2d(
                value=concat, filters=kernel, rate=1, padding='SAME'),
            axis=[2])

    def _init_ctc(self, input_3d):
        """ CTC = Connectionist Temporal Classification. """
        # BxTxC -> TxBxC
        inputs = tf.transpose(input_3d, [1, 0, 2])

        # Ground truth text as sparse tensor
        self._gt_texts = tf.SparseTensor(
            tf.placeholder(tf.int64, shape=[None, 2]),
            tf.placeholder(tf.int32, [None]), tf.placeholder(tf.int64, [2]))

        # Calculate loss for batch
        self._seq_len = tf.placeholder(tf.int32, [None])
        loss = tf.nn.ctc_loss(
            labels=self._gt_texts,
            inputs=inputs,
            sequence_length=self._seq_len,
            ctc_merge_repeated=True)
        decoder = tf.nn.ctc_greedy_decoder(
            inputs=inputs, sequence_length=self._seq_len)
        return (tf.reduce_mean(loss), decoder)

    def _init_tf(self):
        sess = tf.Session()
        saver = tf.train.Saver(max_to_keep=1)
        latest_snapshot = tf.train.latest_checkpoint(StatsModel.MODEL_DIR)

        if self._must_restore and not latest_snapshot:
            raise Exception(f"No saved model found: {StatsModel.MODEL_DIR}")

        if latest_snapshot:
            print(f"Loaded model snapshot: {latest_snapshot}")
            saver.restore(sess, latest_snapshot)
        else:
            print("Created fresh model")
            sess.run(tf.global_variables_initializer())

        return (sess, saver)

    def train_batch(self, batch):
        batch_images = [s.image for s in batch]
        batch_labels = [s.label for s in batch]
        sparse = self._text_mapper.to_sparse(batch_labels)
        feed_dict = {
            self._seq_len: [StatsModel.MAX_CHARS] * len(batch),
            self._input_imgs: batch_images,
            self._gt_texts: sparse,
            self._learning_rate: self._current_learning_rate()
        }
        self._batches_trained += 1
        (_, loss_val) = self._sess.run([self._optimizer, self._loss],
                                       feed_dict)
        return loss_val

    def _current_learning_rate(self):
        """ Rate for current batch, which decays with more training. """
        if (self._batches_trained < 10):
            return 0.01
        elif (self._batches_trained < 10000):
            return 0.001
        else:
            return 0.0001

    def infer_batch(self, batch):
        batch_images = [s.image for s in batch]
        feed_dict = {
            self._seq_len: [StatsModel.MAX_CHARS] * len(batch),
            self._input_imgs: batch_images
        }
        decoded = self._sess.run(self._decoder, feed_dict)
        return self._text_mapper.from_sparse(decoded, len(batch))

    def save(self):
        self._snapshot_id += 1
        self._saver.save(
            self._sess,
            StatsModel.MODEL_DIR + "/stats",
            global_step=self._snapshot_id)
