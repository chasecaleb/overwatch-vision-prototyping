class TextMapper:
    def __init__(self, char_list):
        self._char_list = char_list

    def token_count(self):
        """ Number of possible text characters + CTC blank. """
        return len(self._char_list) + 1

    def to_sparse(self, texts):
        """ Convert batch of texts into sparse tensor for CTC loss. """
        indices = []
        values = []
        shape = [len(texts), 0]
        for (batch_idx, current_text) in enumerate(texts):
            # Convert text (chars) to labels (numbers).
            labels = [self._char_list.index(c) for c in current_text]
            # Sparse tensor must have size of max. label-string
            shape[1] = max(shape[1], len(labels))

            for (label_idx, current_label) in enumerate(labels):
                indices.append([batch_idx, label_idx])
                values.append(current_label)

        return (indices, values, shape)

    def from_sparse(self, ctc_output, batch_size):
        """ Convert CTC output to text. """
        encoded_labels = [[] for _ in range(batch_size)]
        # CTC returns tuple - first element is SparseTensor
        decoded = ctc_output[0][0]

        for (idx, idx_2d) in enumerate(decoded.indices):
            label = decoded.values[idx]
            batch_element = idx_2d[0]
            encoded_labels[batch_element].append(label)

        # Finally, map labels to actual text.
        return [
            str().join([self._char_list[c] for c in encoded])
            for encoded in encoded_labels
        ]
