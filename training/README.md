# TensorFlow-based recognition training

## Setup

- OS packages: `imagemagick pyenv pipenv` (Arch Linux names, Ubuntu/etc may vary)
- Install tensorflow: https://www.tensorflow.org/install/
  - This is not included as a pip package because it is not optimized
- `pipenv --site-packages --python 3.6`
- `pipenv sync`
