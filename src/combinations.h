#pragma once
#include <algorithm>
#include <limits>
#include <vector>

namespace ow {
/**
 * Generates n-choose-k combinations. Indices will be ordered.
 *
 * Adapted from http://rosettacode.org/wiki/Combinations#C.2B.2B
 *
 * @param n Length of list. Must be > 0.
 * @param k Length of each combination. Must be > 0.
 * @return Vectors of combinations (values are element indices). Each inner
 * vector is one combination. Will be empty if k > n.
 */
std::vector<std::vector<int>>
generateCombinations(unsigned long n, unsigned long k);

/**
 * Find best (lowest-score) combination of all possible combinations from input.
 * This will be very slow if input vector is large.
 *
 * @tparam ELEMENT Type of combination element
 * @tparam F Type of score function - should take a vector of elements and
 * return double.
 * @param allElements Input list of elements to generate combinations from.
 * @param combinationSize Size of each combination (as in the "k" of "n choose
 * k").
 * @param scoreFunction Used to determine best combination, where smaller scores
 * = better. If max double, the combination will be skipped.
 * @return Best combination if any is found, otherwise empty.
 */
template<typename ELEMENT, typename F>
std::vector<ELEMENT>
findBestCombination(std::vector<ELEMENT>& allElements,
                    unsigned long combinationSize,
                    F& scoreFunction)
{
  double minScore = std::numeric_limits<double>::max();
  std::vector<ELEMENT> minCombination;

  const auto combinationIndices =
    generateCombinations(allElements.size(), combinationSize);
  for (const auto& indices : combinationIndices) {
    std::vector<ELEMENT> currentCombination(indices.size());
    std::transform(indices.begin(),
                   indices.end(),
                   currentCombination.begin(),
                   [&](auto i) { return allElements[i]; });

    double currentScore = scoreFunction(currentCombination);
    if (currentScore < minScore) {
      minScore = currentScore;
      minCombination = currentCombination;
    }
  }

  return minCombination;
};
}
