#pragma once
#include <opencv2/opencv.hpp>

namespace ow {
struct ProcessParams
{
  int width;
};

const cv::Mat
process(const cv::Mat input, const ProcessParams params);
}
