#include "debugUtil.h"
#ifdef OW_DEBUG
#include <opencv2/cvv.hpp>
#endif

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

void
ow::debug::doShow(cv::Mat image,
                  cvv::impl::CallMetaData location,
                  std::string description)
{
#ifdef OW_DEBUG
  cvv::showImage(image, location, description);
#endif
}

void
ow::debug::doShowVerbose(cv::Mat image,
                         cvv::impl::CallMetaData location,
                         std::string description)
{
#ifdef OW_DEBUG_ALL
  ow::debug::doShow(image, location, description);
#endif
}

void
ow::debug::doShowOverlay(cv::Mat image,
                         std::vector<cv::Rect> overlay,
                         cvv::impl::CallMetaData location,
                         std::string description)
{
#ifdef OW_DEBUG
  cv::Mat output;
  if (image.channels() == 1) {
    cv::cvtColor(image, output, cv::COLOR_GRAY2BGR);
  } else {
    output = image.clone();
  }

  for (const auto& r : overlay) {
    cv::rectangle(output, r, cv::Scalar(0, 0, 255), 2);
  }
  ow::debug::doShow(output, location, description);
#endif
}

void
ow::debug::doShowOverlayVerbose(cv::Mat image,
                                std::vector<cv::Rect> overlay,
                                cvv::impl::CallMetaData location,
                                std::string description)
{
#ifdef OW_DEBUG_ALL
  ow::debug::doShowOverlay(image, overlay, location, description);
#endif
}

#pragma clang diagnostic pop
