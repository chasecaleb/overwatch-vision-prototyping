#pragma once
#include "detectionParams.h"
#include "processing.h"
#include <opencv2/opencv.hpp>

namespace ow {
struct DetectionResults
{
  bool success;
  std::vector<cv::Rect> stats;
  cv::Rect hero;
  cv::Rect outcome;
};

DetectionResults
doDetection(const cv::Mat& processedImage, const ow::DetectionParams& params);
}
