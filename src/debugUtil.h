#pragma once
#include <functional>
#include <opencv2/cvv.hpp>
#include <opencv2/opencv.hpp>

namespace ow {
namespace debug {
void
doShow(cv::Mat image,
       cvv::impl::CallMetaData location,
       std::string description);

void
doShowVerbose(cv::Mat image,
              cvv::impl::CallMetaData location,
              std::string description);

void
doShowOverlay(cv::Mat image,
              std::vector<cv::Rect> overlay,
              cvv::impl::CallMetaData location,
              std::string description);

void
doShowOverlayVerbose(cv::Mat image,
                     std::vector<cv::Rect> overlay,
                     cvv::impl::CallMetaData location,
                     std::string description);
}
}
