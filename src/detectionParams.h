#pragma once
#include "processing.h"
#include <opencv2/opencv.hpp>

namespace ow {
struct StatDetectionParams
{
  int minW, minH, maxW, maxH, deathsOffset, boundsExpansion;
  double singleStatAspectRatio, totalStatsAspectRatio;
  cv::Rect region;
};

struct RegionDetectionParams
{
  int maxW, maxH, margin;
  // Distance from center of hero name to top-center of stats.
  cv::Point statsOffset;
};

struct DetectionParams
{
  StatDetectionParams stats;
  RegionDetectionParams hero;
  RegionDetectionParams outcome;
};

DetectionParams
createDetectionParams(const ow::ProcessParams& processParams);
}
