#include "processing.h"
#include "debugUtil.h"
#include <opencv2/cvv.hpp>

const cv::Mat
ow::process(const cv::Mat input, const ow::ProcessParams params)
{
  cv::Mat resized;
  cv::resize(
    input,
    resized,
    cv::Size(params.width,
             static_cast<int>((double)params.width / input.cols * input.rows)));
  ow::debug::doShowVerbose(resized, CVVISUAL_LOCATION, "original");

  cv::Mat gray;
  cv::cvtColor(resized, gray, cv::COLOR_BGR2GRAY);
  ow::debug::doShowVerbose(gray, CVVISUAL_LOCATION, "gray");

  cv::Mat blurred;
  cv::GaussianBlur(gray, blurred, cv::Size(7, 7), 0);
  ow::debug::doShowVerbose(blurred, CVVISUAL_LOCATION, "blurred");

  cv::Mat tophat;
  cv::morphologyEx(blurred,
                   tophat,
                   cv::MORPH_TOPHAT,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11, 5)));
  ow::debug::doShowVerbose(tophat, CVVISUAL_LOCATION, "tophat");

  cv::Mat gradient, absGradient;
  cv::Scharr(tophat, gradient, CV_32F, 0, 1);
  cv::convertScaleAbs(gradient, absGradient);
  ow::debug::doShowVerbose(absGradient, CVVISUAL_LOCATION, "derivative");

  cv::Mat closed, thresholded;
  cv::morphologyEx(absGradient,
                   closed,
                   cv::MORPH_CLOSE,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5)));
  cv::threshold(
    closed, thresholded, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
  ow::debug::doShowVerbose(closed, CVVISUAL_LOCATION, "closed");
  ow::debug::doShowVerbose(thresholded, CVVISUAL_LOCATION, "thresholded");

  cv::Mat finalClosed, finalOpened;
  cv::morphologyEx(thresholded,
                   finalClosed,
                   cv::MORPH_CLOSE,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 3)));
  ow::debug::doShowVerbose(finalClosed, CVVISUAL_LOCATION, "closed");

  cv::morphologyEx(finalClosed,
                   finalOpened,
                   cv::MORPH_OPEN,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
                   cv::Point(-1, -1),
                   2);
  ow::debug::doShowVerbose(finalOpened, CVVISUAL_LOCATION, "opened");

  return finalOpened;
}
