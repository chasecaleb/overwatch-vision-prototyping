#include "detection.h"
#include "miscDetection.h"
#include "statDetection.h"
#include <opencv2/opencv.hpp>

ow::DetectionResults
ow::doDetection(const cv::Mat& processedImage,
                const ow::DetectionParams& params)
{
  ow::DetectionResults result;
  result.stats = ow::detectStats(processedImage, params);
  if (result.stats.size() != 6) {
    result.success = false;
    return result;
  }

  result.hero = ow::detectRegion(processedImage, params.hero, result.stats);
  result.outcome =
    ow::detectRegion(processedImage, params.outcome, result.stats);
  // Player mic indicators below outcome can make it too big, so constrain it.
  // Fortunately, hero text works as a fairly reliable upper bound.
  result.outcome.height = std::min(result.hero.height, result.outcome.height);

  result.success = true;
  return result;
}
