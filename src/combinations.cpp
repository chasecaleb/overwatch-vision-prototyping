#include "combinations.h"

std::vector<std::vector<int>>
ow::generateCombinations(unsigned long n, unsigned long k)
{
  if (k > n) {
    return std::vector<std::vector<int>>();
  }

  std::vector<std::size_t> bitmask(n);
  for (std::size_t i = 0; i < k; i++) {
    bitmask[i] = 1;
  }
  for (std::size_t i = k; i < n; i++) {
    bitmask[i] = 0;
  }

  std::vector<std::vector<int>> result;
  do {
    std::vector<int> current;
    for (std::size_t i = 0; i < n; i++) {
      if (bitmask[i]) {
        current.push_back(static_cast<int>(i));
      }
    }
    result.push_back(current);
  } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
  return result;
}
