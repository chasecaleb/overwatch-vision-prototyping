#include "miscDetection.h"
#include "calculations.h"
#include "debugUtil.h"

cv::Rect
findOutterBounds(const cv::Mat& roi, int minArea)
{
  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(roi, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

  std::vector<cv::Rect> bounds;
  for (const auto& contour : contours) {
    const auto b = cv::boundingRect(contour);
    // Filter noise (small contours) and anything touching the edges, which
    // were presumably cropped and thus actually too large.
    const auto touchesEdge = b.tl().x == 0 || b.tl().y == 0 ||
                             b.br().x == roi.cols || b.br().y == roi.rows;
    if (b.area() > minArea && !touchesEdge) {
      bounds.push_back(b);
    }
  }

  if (bounds.empty()) {
    // No contours - return a placeholder (aka null object design pattern).
    return cv::Rect(0, 0, 0, 0);
  }

  cv::Rect merged = bounds[0];
  for (const auto& b : bounds) {
    // Union of two rects = merged bounding box.
    merged = merged | b;
  }
  return merged;
}

cv::Point
statsTopCenter(const std::vector<cv::Rect>& stats)
{
  return (ow::calc::getTopCenter(stats[0]) +
          ow::calc::getTopCenter(stats[stats.size() - 1])) *
         0.5;
}

cv::Rect
centerRect(const cv::Point& center, int width, int height)
{
  return cv::Rect(
    center.x - width * 0.5, center.y - height * 0.5, width, height);
}

cv::Rect
ow::detectRegion(const cv::Mat& processedImage,
                 const ow::RegionDetectionParams& params,
                 const std::vector<cv::Rect>& stats)
{
  const auto roiCenter = statsTopCenter(stats) - params.statsOffset;
  auto roiOutline = centerRect(roiCenter, params.maxW, params.maxH);
  // Constrain ROI within image to prevent potential crash.
  // (This shouldn't typically happen, but it may be possible.)
  roiOutline.x = std::max(0, roiOutline.x);
  roiOutline.y = std::max(0, roiOutline.y);
  roiOutline.x -= std::max(0, roiOutline.br().x - processedImage.cols);
  roiOutline.y -= std::max(0, roiOutline.br().y - processedImage.rows);

  const auto roi = processedImage(roiOutline);
  auto contourBounds = findOutterBounds(roi, 0.025 * roiOutline.area());
  if (contourBounds.area() == 0) {
    // Nothing found, abort.
    return contourBounds;
  }

  // Expand bounds to leave a margin around text.
  contourBounds.x -= params.margin;
  contourBounds.y -= params.margin;
  contourBounds.width += 2 * params.margin;
  contourBounds.height += 2 * params.margin;

  ow::debug::doShowOverlayVerbose(
    roi, { contourBounds }, CVVISUAL_LOCATION, "detected region");
  // Bounds were relative to ROI, so need to adjust them.
  return cv::Rect(roiOutline.tl() + contourBounds.tl(), contourBounds.size());
}
