#include "debugUtil.h"
#include "detection.h"
#include "processing.h"
#include <iostream>
#include <opencv2/opencv.hpp>

int
main(int argc, char** argv)
{
  if (argc == 1) {
    std::cout << "Must supply at least one filename as argument(s) \n";
    exit(1);
  }

  ow::ProcessParams processParams;
  processParams.width = 1280;
  const auto detectionParams = ow::createDetectionParams(processParams);

  for (int i = 1; i < argc; i++) {
    const std::string filename = argv[i];
    std::cout << "Processing: " << filename << "\n";

    const auto input = cv::imread(filename);
    const auto processed = process(input, processParams);
    const auto detected = doDetection(processed, detectionParams);

    auto allBounds = detected.stats;
    allBounds.push_back(detected.hero);
    allBounds.push_back(detected.outcome);
    ow::debug::doShowOverlay(input, allBounds, CVVISUAL_LOCATION, "detected");
  }

  cvv::finalShow();
  return 0;
}
