#include "calculations.h"

cv::Point
ow::calc::getTopCenter(const cv::Rect& r)
{
  return cv::Point(r.tl().x + r.width / 2, r.tl().y);
}
