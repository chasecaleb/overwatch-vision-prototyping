#pragma once
#include <opencv2/opencv.hpp>

namespace ow {
namespace calc {
cv::Point
getTopCenter(const cv::Rect& r);
}
}
