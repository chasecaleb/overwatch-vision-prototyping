#pragma once
#include "detection.h"
#include <opencv2/opencv.hpp>

namespace ow {
std::vector<cv::Rect>
detectStats(const cv::Mat& processedImage, const DetectionParams& params);
}
