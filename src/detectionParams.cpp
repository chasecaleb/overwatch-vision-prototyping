#include "detectionParams.h"

ow::DetectionParams
ow::createDetectionParams(const ow::ProcessParams& processParams)
{
  // TODO: scale these measurements based on image dimensions.
  ow::DetectionParams result;
  result.stats.minW = 3;
  result.stats.minH = 20;
  result.stats.maxW = 130;
  result.stats.maxH = 130;
  result.stats.deathsOffset = 5;
  result.stats.boundsExpansion = 4;
  result.stats.totalStatsAspectRatio = 900.0 / 58.0;
  result.stats.singleStatAspectRatio = 2;

  // Screen is expected to be only 85% of image
  const double scaleFactor = 0.85;
  // Assuming aspect ratio is 16:9 (e.g. 1280x720)
  const int imageHeight = processParams.width * 16 / 9;

  const int roiWidth = 1150 * scaleFactor;
  const int roiHeight = 220 * scaleFactor;
  // IMPORTANT: OpenCV's y-axis is flipped (0 = top)
  const int roiCenterY =
    380 * scaleFactor - imageHeight * (1 - scaleFactor) / 2;

  result.stats.region = cv::Rect((processParams.width - roiWidth) / 2,
                                 roiCenterY + roiHeight / 2,
                                 roiWidth,
                                 roiHeight);

  result.hero.statsOffset = cv::Point(375, 120);
  result.hero.maxW = 310;
  result.hero.maxH = 80;
  result.hero.margin = 5;

  result.outcome.statsOffset = cv::Point(395, 210);
  result.outcome.maxW = 375;
  result.outcome.maxH = 110;
  result.outcome.margin = 5;

  return result;
}
