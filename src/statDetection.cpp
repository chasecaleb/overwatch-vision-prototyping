#include "statDetection.h"
#include "calculations.h"
#include "combinations.h"
#include "debugUtil.h"
#include "detection.h"
#include <limits>

bool
validSize(const cv::Rect& bounds, const ow::DetectionParams& params)
{
  return bounds.width < params.stats.maxW &&
         bounds.height < params.stats.maxH &&
         bounds.width > params.stats.minW && bounds.height > params.stats.minH;
}

std::vector<cv::Rect>
findBest(std::vector<cv::Rect> candidates, const ow::DetectionParams& params)
{
  // Sort to make combination scoring simpler and more efficient, since we're
  // looking for stats that are lined up approximately the same distance apart.
  std::sort(candidates.begin(),
            candidates.end(),
            [](const cv::Rect& a, const cv::Rect& b) { return a.x < b.x; });

  const auto calculateScore = [&](std::vector<cv::Rect> combination) {
    std::vector<cv::Point> topCenterPts(combination.size());
    std::transform(combination.begin(),
                   combination.end(),
                   topCenterPts.begin(),
                   ow::calc::getTopCenter);
    // Final stat (deaths) is slightly lower, so adjust it slightly so that
    // scoring isn't affected.
    topCenterPts[5] = topCenterPts[5] - cv::Point(0, params.stats.deathsOffset);

    // Height of bounds measures vertical alignment discrepancy.
    const auto verticalError = cv::boundingRect(topCenterPts).height;

    const int totalDist = topCenterPts[5].x - topCenterPts[0].x;
    const int expectedDist = totalDist / (topCenterPts.size() - 1);
    int distanceError = 0;
    for (std::size_t i = 0; i < topCenterPts.size() - 1; i++) {
      const int dist = topCenterPts[i + 1].x - topCenterPts[i].x;
      distanceError += cv::abs(expectedDist - dist);
    }

    // TODO add threshold to weed out false positives - return max double value
    // if over threshold
    // TODO tune the weight of vertical/distance errors?
    return 2 * verticalError + distanceError;
  };

  return ow::findBestCombination(candidates, 6, calculateScore);
}

std::vector<cv::Rect>
adjustBounds(const std::vector<cv::Rect>& stats,
             const ow::DetectionParams& params)
{
  if (stats.size() != 6) {
    // Not enough stats found, so adjustment calculations won't work (and there
    // isn't any point trying).
    return stats;
  }

  // Calculate stat dimensions based on distance between furthest left/right
  // stats, which should be consistent. This accomplishes two things:
  //
  // 1) Stat values and their names (immediately below) frequently blur together
  // into a single bounding box, but this will effectively crop the name out.
  // 2) Improves machine learning-based text recognition by feeding it regions
  // with the same aspect ratio.
  //
  // Thus, the most reliable way to calculate the new bounds is by using the
  // center point of the top edge. Finally, it is adjusted slightly upwards
  // because the original bounds were calculated using an eroded image.
  const int totalWidth =
    ow::calc::getTopCenter(stats[5]).x - ow::calc::getTopCenter(stats[0]).x;
  const int newHeight = totalWidth / params.stats.totalStatsAspectRatio;
  const int newWidth = newHeight * params.stats.singleStatAspectRatio;
  const auto recalculate = [&](const cv::Rect& r) {
    const auto topCenter =
      cv::Point(ow::calc::getTopCenter(r).x,
                ow::calc::getTopCenter(r).y - params.stats.boundsExpansion);
    return cv::Rect(
      topCenter.x - newWidth / 2, topCenter.y, newWidth, newHeight);
  };
  std::vector<cv::Rect> results;
  for (const auto& s : stats) {
    results.push_back(recalculate(s));
  }
  return results;
}

std::vector<cv::Rect>
ow::detectStats(const cv::Mat& processedImage,
                const ow::DetectionParams& params)
{
  cv::Mat statsRoi = processedImage(params.stats.region);
  ow::debug::doShowVerbose(statsRoi, CVVISUAL_LOCATION, "stats roi");

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(statsRoi, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

  std::vector<cv::Rect> statCandidates;
  for (const auto& contour : contours) {
    const auto bounds = cv::boundingRect(contour);
    if (validSize(bounds, params)) {
      statCandidates.push_back(bounds);
    }
  }
  ow::debug::doShowOverlayVerbose(
    statsRoi, statCandidates, CVVISUAL_LOCATION, "stat candidates");

  const auto best = findBest(statCandidates, params);
  ow::debug::doShowOverlayVerbose(
    statsRoi, best, CVVISUAL_LOCATION, "chosen stats");

  const auto adjusted = adjustBounds(best, params);
  ow::debug::doShowOverlayVerbose(
    statsRoi, adjusted, CVVISUAL_LOCATION, "adjusted bounds");

  // Stat bounds are based on cropped ROI, so need to compensate.
  std::vector<cv::Rect> finalStats(best.size());
  std::transform(adjusted.begin(),
                 adjusted.end(),
                 finalStats.begin(),
                 [&](const cv::Rect& r) {
                   return cv::Rect(params.stats.region.tl() + r.tl(), r.size());
                 });
  return finalStats;
}
