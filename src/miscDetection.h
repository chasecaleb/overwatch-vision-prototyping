#pragma once
#include "detectionParams.h"
#include <opencv2/opencv.hpp>

namespace ow {
cv::Rect
detectHero(const cv::Mat& processedImage,
           const ow::DetectionParams& params,
           const std::vector<cv::Rect>& stats);

cv::Rect
detectRegion(const cv::Mat& processedImage,
             const ow::RegionDetectionParams& params,
             const std::vector<cv::Rect>& stats);
}
