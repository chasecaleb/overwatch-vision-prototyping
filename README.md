# Overwatch Vision: Prototyping/Sandbox

## Getting Started

1. Install OpenCV 3.4.2, **including `contrib` modules**.
   - Linux users: `./install-opencv.sh` should handle this for you.
   - [https://docs.opencv.org/3.4.2/d7/d9f/tutorial_linux_install.html](OpenCV Linux instructions)
1. Checkout submodules: `git submodule init && git submodule update`
1. Build: `cmake . && make`
1. Run: `./bin/overwatch_vision_prototype`
